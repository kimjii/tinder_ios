//
//  ViewController.swift
//  tinder_app
//
//  Created by Jérémy Kim on 12/12/2018.
//  Copyright © 2018 IF26. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerView: UIPickerView!
    let personeees = ["Jérémy","Clara","Stéphane","Allan"]
    var persons : Array<Personne> = Array<Personne>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self

        let db = DatabaseAccessor.sharedInstance
        persons = db.getPersonnes()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return persons.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String {
        return "\(persons[row].prenom) \(persons[row].nom)"
    }

    @IBAction func selectUser(_ sender: Any) {
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        authenticated_user = persons[selectedRow]
    }
    
}

