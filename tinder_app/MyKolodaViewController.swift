//
//  MyKolodaViewController.swift
//  tinder_app
//
//  Created by Jérémy Kim on 04/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import UIKit
import Koloda
import pop
import Alamofire

private let frameAnimationSpringBounciness:CGFloat = 9
private let frameAnimationSpringSpeed:CGFloat = 16
private let kolodaCountOfVisibleCards = 2
private let kolodaAlphaValueSemiTransparent:CGFloat = 0.1

class KolodaPhoto {
    var photoUrlString = ""
    var title = ""
    var distance = ""
    var id : Int
    
    init(title: String, url: String, id: Int, distance: String) {
        self.title = title
        photoUrlString = url
        self.id = id
        self.distance = distance
    }
}

class MyKolodaViewController: UIViewController, KolodaViewDataSource, KolodaViewDelegate {
    
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return self.photos.count
    }
    
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let photoView = Bundle.main.loadNibNamed("KolodaPhotoView",
                                                 owner: self, options: nil)![0] as? KolodaPhotoView
        let photo = photos[Int(index)]
        let dataDecoded : Data = Data(base64Encoded: photo.photoUrlString, options: .ignoreUnknownCharacters)!
        let decodedimage = UIImage(data: dataDecoded)
        photoView?.photoImageView?.image = decodedimage
        photoView?.photoTitleLabel?.text = photo.title
        photoView?.distanceLabel?.text = photo.distance
        return photoView!
    }
    
    
    @IBOutlet weak var kolodaView: CustomKolodaView!
    
    var photos = Array<KolodaPhoto>()
    var db: DatabaseAccessor = DatabaseAccessor.sharedInstance
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
        kolodaView.dataSource = self
        kolodaView.delegate = self
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.db = DatabaseAccessor.sharedInstance
        fetchPhotos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if swipe_direction == Koloda.SwipeResultDirection.right {
            self.rightButtonTapped()
        } else if swipe_direction == Koloda.SwipeResultDirection.left {
            self.leftButtonTapped()
        }
        swipe_direction = nil
    }
    
    //MARK: Datahandling
    func fetchPhotos() {
        let personnes = self.db.getPersonsWithinSettingsRange(person_id: authenticated_user.id)
        for personne in personnes {
            //TODO: add distance to card
            let distance = Utils.getDistance(lat1: authenticated_user.latitude, long1: authenticated_user.longitude, lat2: (personne.latitude), long2: (personne.longitude))
            let distanceLabel = String(distance) + " km"
            let title = personne.prenom + ", " + String(Utils.getAge(birthDate: personne.date_naissance))
            let photo = KolodaPhoto(title: title, url: personne.photos[0], id: personne.id, distance: distanceLabel)
            self.photos.append(photo)
            self.kolodaView.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func leftButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.left)
    }
    
    @IBAction func rightButtonTapped() {
        kolodaView?.swipe(SwipeResultDirection.right)
    }
    
    //MARK: KolodaViewDataSource
    func kolodaNumberOfCards(koloda: KolodaView) -> UInt {
        return UInt(self.photos.count)
    }
    
    func kolodaViewForCardAtIndex(koloda: KolodaView, index: UInt) -> UIView {
        let photoView = Bundle.main.loadNibNamed("KolodaPhotoView",
                                                       owner: self, options: nil)![0] as? KolodaPhotoView
        let photo = photos[Int(index)]
        photoView?.photoImageView?.imageFromUrl(urlString: photo.photoUrlString)
        photoView?.photoTitleLabel?.text = photo.title
        return photoView!
    }
    func kolodaViewForCardOverlayAtIndex(koloda: KolodaView, index: UInt) -> OverlayView? {
        return Bundle.main.loadNibNamed("CustomOverlayView",
                                              owner: self, options: nil)![0] as? OverlayView
    }
    
    //MARK: KolodaViewDelegate
    
    func koloda(_ koloda: Koloda.KolodaView, didSwipeCardAt index: Int, in direction: Koloda.SwipeResultDirection) {
        if (direction == Koloda.SwipeResultDirection.right) {
            let match = self.db.likeWithPersonId(person_id: authenticated_user.id, liked_person_id: self.photos[index].id)
            
            if (match) {
                let alert = UIAlertController(title: "C'est un match !", message: "Voulez-vous voir son profile ?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Allons-y Alonzo", style: .default, handler: { action in
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "detailedCard") as! DetailedCardViewController
                    newViewController.personId = self.photos[index].id
                    newViewController.hideButton = true
                    self.navigationController?.pushViewController(newViewController, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Abort", style: .cancel, handler: nil))
                
                self.present(alert, animated: true)
            }
        }
    }
    
    func kolodaDidRunOutOfCards(koloda: KolodaView) {
        //Example: reloading
        //kolodaView.resetCurrentCardNumber()
        //fetchPhotos()
    }
    
    func koloda(_ koloda: Koloda.KolodaView, didSelectCardAt index: Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "detailedCard") as! DetailedCardViewController
        newViewController.personId = self.photos[index].id
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func kolodaShouldApplyAppearAnimation(koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldMoveBackgroundCard(koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldTransparentizeNextCard(koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaBackgroundCardAnimation(koloda: KolodaView) -> POPPropertyAnimation? {
        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        animation!.springBounciness = frameAnimationSpringBounciness
        animation!.springSpeed = frameAnimationSpringSpeed
        return animation
    }
    
}
