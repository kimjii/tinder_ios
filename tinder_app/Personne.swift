//
//  Personne.swift
//  tinder_app
//
//  Created by Clara Garbe Ortega on 09/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import Foundation

class Personne {
    var id: Int
    var nom: String
    var prenom: String
    var date_naissance: String // jj/mm/aaaa
    var sexe: String // m ou f
    var description: String
    var longitude: Double
    var latitude: Double
    var preference: Int //0 homme, 1 femme, 2 les deux
    var age_min: Int
    var age_max: Int
    var distance_max: Int
    var liked_persons: Array<Personne> = Array()
    var photos: Array<String> = Array()
    
    init(){
        self.id=0
        self.nom = "nom"
        self.prenom = "prenom"
        self.date_naissance = "date_naissance"
        self.sexe = "sexe"
        self.description = "description"
        self.longitude = 0.0
        self.latitude = 0.0
        self.preference = 2
        self.age_min = 18
        self.age_max = 50
        self.distance_max = 15
        self.liked_persons = Array<Personne>()
        self.photos = Array<String>()
    }
    
    init(id: Int, nom: String, prenom: String, date_naissance: String, sexe: String, description: String, longitude: Double, latitude: Double, preference: Int, age_min: Int, age_max: Int,
         distance_max: Int){
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.date_naissance = date_naissance
        self.sexe = sexe
        self.description = description
        self.longitude = longitude
        self.latitude = latitude
        self.preference = preference
        self.age_min = age_min
        self.age_max = age_max
        self.distance_max = distance_max
        self.liked_persons = Array<Personne>()
        self.photos = Array<String>()
    }
    
    //faire un set liked persons et un set photos

    
    public var descriptor: String {
        var toReturn : String = "Personne id \(id) \(nom), \(prenom), \(date_naissance), \(sexe), \(description), longitude: \(longitude), latitude: \(latitude), preference: \(preference), ages : \(age_min) - \(age_max), distance max: \(distance_max) likes"
        if(liked_persons.count == 0){
            toReturn += " no one"
        }
        else{
            for personne in liked_persons{
                toReturn += " \(personne.prenom) ."
            }
        }
        
        
        return toReturn
    }
}
