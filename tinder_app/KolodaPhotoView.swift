//
//  KolodaPhotoView.swift
//  tinder_app
//
//  Created by Jérémy Kim on 06/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import UIKit

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(url: url as URL)
            NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: OperationQueue.main, completionHandler: {[weak self] response, data, error in
                if let data = data {
                    self?.image = UIImage(data: data)
                }
            })
        }
    }
}

class KolodaPhotoView: UIView {
    
    @IBOutlet var photoImageView: UIImageView?
    @IBOutlet var photoTitleLabel: UILabel?
    @IBOutlet var distanceLabel: UILabel?
    
}
