//
//  DetailedCardViewController.swift
//  tinder_app
//
//  Created by Jérémy Kim on 12/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import UIKit
import ImageSlideshow
import Koloda

class DetailedCardViewController: UIViewController {

    @IBOutlet var slideShow: ImageSlideshow!
    @IBOutlet weak var titreLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var dislikeButton: UIButton!
    
    var personId = 0
    var hideButton = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let db = DatabaseAccessor.sharedInstance
        let personne = db.getPersonFromId(person_id: personId)
        titreLabel.text = (personne?.prenom)! + ", " + String(Utils.getAge(birthDate: (personne?.date_naissance)!))
        let distance = Utils.getDistance(lat1: authenticated_user.latitude, long1: authenticated_user.longitude, lat2: (personne?.latitude)!, long2: (personne?.longitude)!)
        distanceLabel.text = String(distance) + " km"
        descriptionLabel.text = personne?.description
        slideShow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        var photos = Array<ImageSource>()
        for photo in (personne?.photos)! {
            let dataDecoded : Data = Data(base64Encoded: photo, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            photos.append(ImageSource(image: decodedimage!))
        }
        slideShow.setImageInputs(photos)
        
        likeButton.isHidden = hideButton
        dislikeButton.isHidden = hideButton
        
    }
    
    @IBAction func dislike(_ sender: Any) {
        swipe_direction = Koloda.SwipeResultDirection.left
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func like(_ sender: Any) {
        swipe_direction = Koloda.SwipeResultDirection.right
        self.navigationController?.popViewController(animated: true)

    }
    
}
