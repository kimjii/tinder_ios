//
//  DatabaseAccessor.swift
//  tinder_app
//
//  Created by Clara Garbe Ortega on 09/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import Foundation
import SQLite

class DatabaseAccessor {
    static let sharedInstance = DatabaseAccessor()
    
    //
    private var database: Connection!
    
    static let table_personnes = Table("personnes")
    static let personnes_personne_id = Expression<Int>("personne_id")
    static let personnes_nom = Expression<String>("nom")
    static let personnes_prenom = Expression<String>("prenom")
    static let personnes_date_naissance = Expression<String>("date_naissance")
    static let personnes_sexe = Expression<String>("sexe")
    static let personnes_description = Expression<String>("description")
    static let personnes_longitude = Expression<Double>("longitude")
    static let personnes_latitude = Expression<Double>("lagitude")
    static let personnes_preference = Expression<Int>("preference")
    static let personnes_age_min = Expression<Int>("age_min")
    static let personnes_age_max = Expression<Int>("age_max")
    static let personnes_distance_max = Expression<Int>("distance_max")
    
    static let table_matchs = Table("matchs")
    static let matchs_liking_person_id = Expression<Int>("liking_person_id")
    static let matchs_liked_person_id = Expression<Int>("liked_person_id")
    static let matchs_timestamp = Expression<String>("timestamp")
    
    static let table_photos = Table("photos")
   // static let photos_photo_id = Expression<Int>("photo_id") c'est mieux une clé composée de person id et photo position je pense
    static let photos_fk_personne_id = Expression<Int>("personne_id")
    static let photos_photo_position = Expression<Int>("photo_position")
    static let photos_photo = Expression<String>("photo")
    
    private init(){
        print("-- initializing database")
        
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create:               true)
            let fileUrl = documentDirectory.appendingPathComponent("tinder_database").appendingPathExtension("sqlite3")
            let base = try Connection(fileUrl.path)
            self.database = base;
        }
        catch {
            print (error)
        }
        
        //createTables()
        //populateDatabase()
        //resetDatabase()
        
        print("-- done initializing database")

    }
    
    func resetDatabase(){
        let dropTablePersonnes = DatabaseAccessor.table_personnes.drop(ifExists: true)
        let dropTableMatchs = DatabaseAccessor.table_matchs.drop(ifExists: true)
        let dropTablePhotos = DatabaseAccessor.table_photos.drop(ifExists: true)

        do{
            try self.database.run(dropTablePersonnes)
            try self.database.run(dropTableMatchs)
            try self.database.run(dropTablePhotos)
            print("----Deleted tables successfully")
        }
        catch {
            print (error)
        }
        createTables()
        populateDatabase()
    }
    
    func createTables(){
        createTablePersonnes()
        createTableMatchs()
        createTablePhotos()
    }
    
    func createTablePersonnes(){
        print("----- createTablePersonnes debut")
            // Instruction pour faire un create de la table USERS
        let createTablePersonnes = DatabaseAccessor.table_personnes.create(ifNotExists: true) { table in
                table.column(DatabaseAccessor.personnes_personne_id, primaryKey: .autoincrement )
                table.column(DatabaseAccessor.personnes_nom)
                table.column(DatabaseAccessor.personnes_prenom)
                table.column(DatabaseAccessor.personnes_date_naissance)
                table.column(DatabaseAccessor.personnes_sexe)
                table.column(DatabaseAccessor.personnes_description)
                table.column(DatabaseAccessor.personnes_longitude)
                table.column(DatabaseAccessor.personnes_latitude)
                table.column(DatabaseAccessor.personnes_preference)
                table.column(DatabaseAccessor.personnes_age_min)
                table.column(DatabaseAccessor.personnes_age_max)
                table.column(DatabaseAccessor.personnes_distance_max)
        }
        do {
                // Exécution du drop et du create
                //try self.database.run(dropTable)
            try self.database.run(createTablePersonnes)
            print ("----- Table personnes est créée")
        }
        catch {
            print (error)
        }
        print ("----- createTablePersonnes fin")
    }
    
    func createTableMatchs(){
        print("----- createTableMatchs debut")
            // Instruction pour faire un create de la table USERS
        let createTableMatchs = DatabaseAccessor.table_matchs.create(ifNotExists: true) { table in
                table.column(DatabaseAccessor.matchs_liking_person_id, references: DatabaseAccessor.table_personnes, DatabaseAccessor.personnes_personne_id )
                table.column(DatabaseAccessor.matchs_liked_person_id, references: DatabaseAccessor.table_personnes, DatabaseAccessor.personnes_personne_id)
                table.column(DatabaseAccessor.matchs_timestamp)
                table.primaryKey(DatabaseAccessor.matchs_liking_person_id, DatabaseAccessor.matchs_liked_person_id)
        }
        do {
            // Exécution du drop et du create
            //try self.database.run(dropTable)
            try self.database.run(createTableMatchs)
            print ("----- Table matchs est créée")
        }
        catch {
            print (error)
        }
        
        print ("----- createTableMatchs fin")
    }
    
    func createTablePhotos(){
        print("----- createTablePhotos debut")

            // Instruction pour faire un create de la table USERS
        let createTablePhotos = DatabaseAccessor.table_photos.create(ifNotExists: true) { table in
                table.column(DatabaseAccessor.photos_fk_personne_id, references: DatabaseAccessor.table_personnes, DatabaseAccessor.personnes_personne_id)
                table.column(DatabaseAccessor.photos_photo_position)
                table.column(DatabaseAccessor.photos_photo)
            table.primaryKey(DatabaseAccessor.photos_fk_personne_id, DatabaseAccessor.photos_photo_position)
        }
        do {
            // Exécution du drop et du create
            //try self.database.run(dropTable)
            try self.database.run(createTablePhotos)
            print ("----- Table photos est créée")
        }
        catch {
            print (error)
        }
        print ("----- createTableMatchs fin")
    }
    
    func insertPersonne(_ personne: Personne) {
        print("------ inserting \(personne.nom) \(personne.prenom) in table personnes")
        let insert = DatabaseAccessor.table_personnes.insert(DatabaseAccessor.personnes_nom <- personne.nom, DatabaseAccessor.personnes_prenom <- personne.prenom, DatabaseAccessor.personnes_date_naissance <- personne.date_naissance, DatabaseAccessor.personnes_sexe <- personne.sexe, DatabaseAccessor.personnes_description <- personne.description, DatabaseAccessor.personnes_longitude <- personne.longitude, DatabaseAccessor.personnes_latitude <- personne.latitude, DatabaseAccessor.personnes_preference <- personne.preference, DatabaseAccessor.personnes_age_min <- personne.age_min, DatabaseAccessor.personnes_age_max <- personne.age_max, DatabaseAccessor.personnes_distance_max <- personne.distance_max )
    
        do {
            try database.run(insert)
            print("inserted alright")
        }
        catch {
            print(error)
        }
        print("------- done inserting \(personne.nom) \(personne.prenom)")
    }
    
    func insertMatch(liking_person_id: Int, liked_person_id:Int){
        print("------ inserting match : id \(liking_person_id) liked  \(liked_person_id) in table matchs")
        let insert = DatabaseAccessor.table_matchs.insert(DatabaseAccessor.matchs_liking_person_id <- liking_person_id, DatabaseAccessor.matchs_liked_person_id <- liked_person_id, DatabaseAccessor.matchs_timestamp <- Date().description)
        do {
            try database.run(insert)
            print("inserted match alright")
        }
        catch {
            print(error)
        }
        print("------- done inserting match")
    }
    
    func insertPhoto(person_id: Int, photo_position: Int, encoded_picture: String){
        print("------ inserting photo nb \(photo_position)of person id \(person_id)")
        let insert = DatabaseAccessor.table_photos.insert(DatabaseAccessor.photos_fk_personne_id <- person_id, DatabaseAccessor.photos_photo_position <- photo_position, DatabaseAccessor.photos_photo <- encoded_picture)
        do {
            try database.run(insert)
            print("inserted picture alright")
        }
        catch {
            print(error)
        }
        print("------- done inserting photo")
    }
    
    func populateDatabase(){
        print("-- started populating database")
        print("--- reading file persons_data.csv")
        let parsedPersonsCSV = getDoubleDimensionArrayForFile(fileName: "persons_data")
            
        for i in 1...parsedPersonsCSV.count-1{
            if(parsedPersonsCSV[i].count == 11){//on donne un id0 mais ce sera overwrite par le autoincrement dans la BD
                let personneToAdd : Personne = Personne.init(id: 0, nom: parsedPersonsCSV[i][0], prenom: parsedPersonsCSV[i][1], date_naissance: parsedPersonsCSV[i][2], sexe: parsedPersonsCSV[i][3], description: parsedPersonsCSV[i][4], longitude: Double(parsedPersonsCSV[i][5])!, latitude: Double(parsedPersonsCSV[i][6])!, preference: Int(parsedPersonsCSV[i][7])!, age_min: Int(parsedPersonsCSV[i][8])!, age_max: Int(parsedPersonsCSV[i][9])!, distance_max: Int(parsedPersonsCSV[i][10])!)
                //print("persontoinsert \(personneToAdd.descriptor)")
                    insertPersonne(personneToAdd)
            }
        }
        print("--- done with file persons_data.csv")

        print("--- reading file matchs_data.csv")
        let parsedMatchsCSV = getDoubleDimensionArrayForFile(fileName: "matchs_data")
        for i in 1...parsedMatchsCSV.count-1{
            if(parsedMatchsCSV[i].count == 2){
                let liking_person : Int = Int(parsedMatchsCSV[i][0])!
                let liked_person : Int = Int(parsedMatchsCSV[i][1])!
               insertMatch(liking_person_id: liking_person, liked_person_id: liked_person)
            }
        }
        print("--- done with file matchs_data.csv")

        print("--- reading file photos_data.csv")
        let parsedPhotosCSV = getDoubleDimensionArrayForFile(fileName: "photos_data")
        for i in 1...parsedPhotosCSV.count-1{
            if(parsedPhotosCSV[i].count == 3){
                let person_id : Int = Int(parsedPhotosCSV[i][0])!
                let photo_position : Int = Int(parsedPhotosCSV[i][1])!
                insertPhoto(person_id: person_id, photo_position: photo_position, encoded_picture: parsedPhotosCSV[i][2])
            }
        }
        print("--- done with file photos_data.csv")

 
        print("-- ended populating database")
    }
    
    func getDoubleDimensionArrayForFile(fileName: String) -> Array<Array<String>> {
        var parsedCSV: [[String]] = []
        if let path = Bundle.main.path(forResource: fileName, ofType: "csv"){
            var data = try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
            let rows = data.components(separatedBy: "\n")
            for row in rows {
                let columns = row.components(separatedBy: ";")
                parsedCSV.append(columns)
            }
        }
        return parsedCSV

    }
    
    func getPersonnes() -> Array<Personne>{
        print("------ Getting personnes from table personne")
        var toReturn = Array<Personne>()
        do{
            let personnes = try self.database.prepare(DatabaseAccessor.table_personnes)
            //personnes.order(DatabaseAccessor.personnes_nom)
            for personne in personnes{
                let personneToAdd : Personne = Personne.init(id: personne[DatabaseAccessor.personnes_personne_id] ,nom: personne[DatabaseAccessor.personnes_nom], prenom: personne[DatabaseAccessor.personnes_prenom], date_naissance: personne[DatabaseAccessor.personnes_date_naissance], sexe: personne[DatabaseAccessor.personnes_sexe], description: personne[DatabaseAccessor.personnes_description], longitude: personne[DatabaseAccessor.personnes_longitude], latitude: personne[DatabaseAccessor.personnes_latitude], preference: personne[DatabaseAccessor.personnes_preference], age_min: personne[DatabaseAccessor.personnes_age_min], age_max: personne[DatabaseAccessor.personnes_age_max], distance_max: personne[DatabaseAccessor.personnes_distance_max])
                
                let likedPersons = getLikedPersonsFromPersonId(liking_person_id: personneToAdd.id)
                personneToAdd.liked_persons = likedPersons
                
                let photos = getPhotosFromPersonId(person_id: personneToAdd.id)
                personneToAdd.photos = photos
                
                toReturn.append(personneToAdd)
            }
        }
        catch{
            print(error)
        }
        print("------ Finished getting personnes from table personne")
        return toReturn
    }
    
    func getPersonFromId(person_id: Int) -> Personne? {
        do{
            let personToReturn : Personne = Personne.init()

            let filterQuery = DatabaseAccessor.table_personnes.filter(DatabaseAccessor.personnes_personne_id == person_id)
            let personne = try self.database.pluck(filterQuery)
            if(personne != nil){
                personToReturn.id = personne![DatabaseAccessor.personnes_personne_id]
                personToReturn.nom = personne![DatabaseAccessor.personnes_nom]
                personToReturn.prenom = personne![DatabaseAccessor.personnes_prenom]
                personToReturn.date_naissance = personne![DatabaseAccessor.personnes_date_naissance]
                personToReturn.sexe = personne![DatabaseAccessor.personnes_sexe]
                personToReturn.description = personne![DatabaseAccessor.personnes_description]
                personToReturn.longitude = personne![DatabaseAccessor.personnes_longitude]
                personToReturn.latitude = personne![DatabaseAccessor.personnes_latitude]
                personToReturn.preference = personne![DatabaseAccessor.personnes_preference]
                personToReturn.age_min = personne![DatabaseAccessor.personnes_age_min]
                personToReturn.age_max = personne![DatabaseAccessor.personnes_age_max]
                personToReturn.distance_max = personne![DatabaseAccessor.personnes_distance_max]
                
                let likedPersons = getLikedPersonsFromPersonId(liking_person_id: person_id)
                personToReturn.liked_persons = likedPersons
                
                let photos = getPhotosFromPersonId(person_id: personToReturn.id)
                personToReturn.photos = photos
                
                print(personToReturn.descriptor)
                return personToReturn
            }
        }
        catch{
            print(error)
        }

        
        //get liked persons from id and add it to person to return
        return nil
    }
    //Attention, l'array des liked persons ne contient ni les liked personnes ni les photos
    func getLikedPersonsFromPersonId(liking_person_id: Int) -> Array<Personne> {
        var likedPersons = Array<Personne>()
        let query = DatabaseAccessor.table_matchs.filter(DatabaseAccessor.matchs_liking_person_id == liking_person_id)
            .join(.leftOuter, DatabaseAccessor.table_personnes, on: DatabaseAccessor.table_matchs[DatabaseAccessor.matchs_liked_person_id] == DatabaseAccessor.table_personnes[DatabaseAccessor.personnes_personne_id])
        do{
            let returnedFromDB = try self.database.prepare(query)
            for personne in returnedFromDB{
                let personToAdd : Personne = Personne.init(id: personne[DatabaseAccessor.personnes_personne_id], nom: personne[DatabaseAccessor.personnes_nom], prenom: personne[DatabaseAccessor.personnes_prenom], date_naissance: personne[DatabaseAccessor.personnes_date_naissance], sexe: personne[DatabaseAccessor.personnes_sexe], description: personne[DatabaseAccessor.personnes_description], longitude: personne[DatabaseAccessor.personnes_longitude], latitude: personne[DatabaseAccessor.personnes_latitude], preference: personne[DatabaseAccessor.personnes_preference], age_min: personne[DatabaseAccessor.personnes_age_min], age_max: personne[DatabaseAccessor.personnes_age_max], distance_max: personne[DatabaseAccessor.personnes_distance_max])
               likedPersons.append(personToAdd)
            }
        }
        catch{
            print(error)
        }
        return likedPersons
    }
    
    func getPhotosFromPersonId(person_id: Int) -> Array<String> {
        var photos = Array<String>()
        let query = DatabaseAccessor.table_photos.filter(DatabaseAccessor.photos_fk_personne_id == person_id)
            .order(DatabaseAccessor.photos_photo_position.asc)
        do{
            let returnedFromDB = try self.database.prepare(query)
            for photo in returnedFromDB{
                let photoToAdd : String = photo[DatabaseAccessor.photos_photo]
                photos.append(photoToAdd)
            }
        }
        catch{
            print(error)
        }
    
        return photos
    }
    
    func getPersonsWithinSettingsRange(person_id: Int) -> Array<Personne> {
        let person_searching : Personne = getPersonFromId(person_id: person_id)!
        let allUsers : Array<Personne> = getPersonnes()
        var personsWithinSettingsRange = Array<Personne>()
        
        for person in allUsers{
            if(person.id != person_id){
                if(checkIfWithinSettings(person_searching: person_searching, person_toCheck: person) && checkIfWithinSettings(person_searching: person, person_toCheck: person_searching)){
                    var alreadyLiked : Bool = false
                    for likedPerson in person_searching.liked_persons{
                        if(likedPerson.id == person.id){
                            alreadyLiked = true
                        }
                    }
                    if(!alreadyLiked){
                        personsWithinSettingsRange.append(person)
                    }
                }
            }
        }
        for row in personsWithinSettingsRange{
            print(" settingsrange ::: \(row.descriptor)")
        }
        return personsWithinSettingsRange
    }
    
    func checkIfWithinSettings(person_searching: Personne, person_toCheck: Personne) -> Bool {
        var sexRange: Bool = false
        var ageRange: Bool = false
        
        switch person_searching.preference{
        case 0:
            if(person_toCheck.sexe == "m"){
                sexRange = true;
            }
            break;
        case 1:
            if(person_toCheck.sexe == "m"){
                sexRange = true
            }
            break;
        default: //case 2
            sexRange = true;
            break;
        }
        if(sexRange){
            let likedPersonAge = Utils.getAge(birthDate: person_toCheck.date_naissance)
            if(likedPersonAge <= person_searching.age_max && likedPersonAge >= person_searching.age_min){
                ageRange = true;
            }
        }
        if(ageRange){
            let distance: Int = Utils.getDistance(lat1: person_searching.latitude, long1: person_searching.longitude, lat2: person_toCheck.latitude, long2: person_toCheck.longitude)
            if(distance <= person_searching.distance_max){
                return true
            }
        }
        return false;
    }
    
    func likeWithPersonId(person_id: Int, liked_person_id: Int) -> Bool {
        insertMatch(liking_person_id: person_id, liked_person_id: liked_person_id)
        return checkMatch(liking_person_id: person_id, liked_person_id: liked_person_id)
    }
    
    func checkMatch(liking_person_id: Int, liked_person_id: Int) -> Bool {
        let likedPersonsOfLikedPerson = getLikedPersonsFromPersonId(liking_person_id: liked_person_id)
        for likedPersonOfLikedPerson in likedPersonsOfLikedPerson{
            if(likedPersonOfLikedPerson.id == liking_person_id){
                return true
            }
        }
        return false;
    }
    
    func getMatchFromPersonId(person_id: Int) -> Array<Personne> {
        var matchs =  Array<Personne>()
        let likedPersons = getLikedPersonsFromPersonId(liking_person_id: person_id)
        for likedPerson in likedPersons{
            if(checkMatch(liking_person_id: person_id, liked_person_id: likedPerson.id)){
                matchs.append(likedPerson)
            }
        }
        return matchs
    }
    
    func updateUser(user_id: Int, new_values: Personne){
        let user = DatabaseAccessor.table_personnes.filter(DatabaseAccessor.personnes_personne_id == user_id)
        let query = user.update([DatabaseAccessor.personnes_prenom <- new_values.prenom, DatabaseAccessor.personnes_description <- new_values.description, DatabaseAccessor.personnes_preference <- new_values.preference, DatabaseAccessor.personnes_age_min <- new_values.age_min, DatabaseAccessor.personnes_age_max <- new_values.age_max, DatabaseAccessor.personnes_distance_max <- new_values.distance_max])
        do{
            try self.database.run(query)
        }
        catch{
            print(error)
        }
        
        print(getPersonFromId(person_id: user_id)!.descriptor)
    }
    
    func updatePictureAtPosition(person_id: Int, photo_position: Int, encoded_photo: String){
        print("updating photo number \(photo_position) of person \(person_id)")
        let photo_row = DatabaseAccessor.table_photos.filter(DatabaseAccessor.photos_fk_personne_id == person_id && DatabaseAccessor.photos_photo_position == photo_position)
        let query = photo_row.update(DatabaseAccessor.photos_photo <- encoded_photo)
        do{
            try self.database.run(query)
        }
        catch{
            print(error)
        }
        print("done updating photo")
    }
    
    func deletePictureAtPosition(person_id: Int, photo_position: Int){
        print("deleting photo number \(photo_position) of person \(person_id)")
        let photo_row = DatabaseAccessor.table_photos.filter(DatabaseAccessor.photos_fk_personne_id == person_id && DatabaseAccessor.photos_photo_position == photo_position)
        let query = photo_row.delete()
        do{
            try self.database.run(query)
        }
        catch{
            print(error)
        }
        print("done updating photo")
    }
    
    func updatePicturesOfUser(person_id: Int, initial_pictures: Array<String>, new_pictures: Array<String>){
        for i in 0...3 {
            if(initial_pictures.count <= new_pictures.count){//a juste update ou inséré des photos
                if(initial_pictures.indices.contains(i)){
                    updatePictureAtPosition(person_id: person_id, photo_position: i+1, encoded_photo: new_pictures[i])
                }
                else{
                    if(new_pictures.indices.contains(i)){
                        insertPhoto(person_id: person_id, photo_position: i+1, encoded_picture: new_pictures[i])
                    }
                }
            }
            else{// a supprimé des photos (car initial picture.count > new pictures.count
                if(new_pictures.indices.contains(i)){
                    updatePictureAtPosition(person_id: person_id, photo_position: i+1, encoded_photo: new_pictures[i])
                }
                else{
                    if(initial_pictures.indices.contains(i)){
                        deletePictureAtPosition(person_id: person_id, photo_position: i+1)
                    }
                }
            }
        }
    }
    
}
