//
//  Utils.swift
//  tinder_app
//
//  Created by Clara Garbe Ortega on 11/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import Foundation
import CoreLocation

class Utils {
    static func getAge(birthDate: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let birthdayDate = dateFormatter.date(from: birthDate)
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar!.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    static func getDistance(lat1: Double, long1: Double, lat2: Double, long2: Double) -> Int {
        let coord1 = CLLocation(latitude: lat1, longitude: long1)
        let coord2 = CLLocation(latitude: lat2, longitude: long2)
        
        let distanceInKm = coord1.distance(from: coord2)/1000
        
        return Int(distanceInKm)+1
    }
}
