//
//  SettingsViewController.swift
//  tinder_app
//
//  Created by Clara Garbe Ortega on 11/01/2019.
//  Copyright © 2019 IF26. All rights reserved.
//

import UIKit
import WARangeSlider


class SettingsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var thirdImageView: UIImageView!
    @IBOutlet weak var fourthImageView: UIImageView!
    var imagePosition : Int = 0
    var modifiedPhotosArray = Array<UIImage>()
    var imagesFilled = [false, false, false, false]
    
    let imagePicker = UIImagePickerController()
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var descriptionField: UITextView!
    
    @IBOutlet weak var hommeCheckbox: UIButton!
    @IBOutlet weak var femmeCheckbox: UIButton!
    var uncheckedBox = UIImage(named: "unchecked_box")
    var checkedBox = UIImage(named: "checked_box")
    var hommeCheckboxChecked: Bool = false
    var femmeCheckboxChecked: Bool! = false
    var selectedPreference : Int?
    
    @IBOutlet weak var ageRangeSlider: RangeSlider!
    @IBOutlet weak var ageMinLabel: UILabel!
    @IBOutlet weak var ageMaxLabel: UILabel!
    
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var updatedValues: Personne = Personne.init()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        nameTextField.text = authenticated_user.prenom
        descriptionField.text = authenticated_user.description
        
         selectedPreference = authenticated_user.preference
        switch selectedPreference{
        case 0: // case homme
            hommeCheckboxChecked = true
            hommeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            break;
        case 1: // case femme
            femmeCheckboxChecked = true
            femmeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            break;
        default:// case les 2
            hommeCheckboxChecked = true
            hommeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            femmeCheckboxChecked = true
            femmeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            break;
        }
        
        ageRangeSlider.lowerValue = Double(authenticated_user.age_min)
        ageMinLabel.text = String(authenticated_user.age_min)
        ageRangeSlider.upperValue = Double(authenticated_user.age_max)
        ageMaxLabel.text = String(authenticated_user.age_max)
        
        distanceSlider.value = Float(authenticated_user.distance_max)
        distanceLabel.text = "\(authenticated_user.distance_max) km"
        
        if(authenticated_user.photos.count >= 1){
            firstImageView.image = returnImageFromEncodedString(encodedString: authenticated_user.photos[0])
            modifiedPhotosArray.append(firstImageView!.image!)
            imagesFilled[0] = true
        }
        if(authenticated_user.photos.count >= 2){
            secondImageView.image = returnImageFromEncodedString(encodedString: authenticated_user.photos[1])
            modifiedPhotosArray.append(secondImageView!.image!)
            imagesFilled[1] = true

        }
        if(authenticated_user.photos.count >= 3){
            thirdImageView.image = returnImageFromEncodedString(encodedString: authenticated_user.photos[2])
            modifiedPhotosArray.append(thirdImageView!.image!)
            imagesFilled[2] = true

        }
        if(authenticated_user.photos.count >= 4){
            fourthImageView.image = returnImageFromEncodedString(encodedString: authenticated_user.photos[3])
            modifiedPhotosArray.append(fourthImageView!.image!)
            imagesFilled[3] = true

        }
        
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func returnImageFromEncodedString(encodedString: String) -> UIImage{
        let imageData = Data(base64Encoded: encodedString, options: .ignoreUnknownCharacters)
        let image = UIImage(data: imageData! as Data)
        return image!
    }

    @IBAction func addFirstImage(_ sender: Any) {
        imagePosition = 0
        clickToUpload()
    }
    
    @IBAction func addSecondImage(_ sender: Any) {
        imagePosition = 1
        clickToUpload()
    }
    
    @IBAction func addThirdImage(_ sender: Any) {
        imagePosition = 2
        clickToUpload()
    }
    
    @IBAction func addFourthImage(_ sender: Any) {
        imagePosition = 3
        clickToUpload()
    }
    
    @IBAction func deleteFirstImage(_ sender: Any) {
        imagePosition = 0
        replacePicturesWhenDelete()
        
    }
    
    @IBAction func deleteSecondImage(_ sender: Any) {
        imagePosition = 1
        replacePicturesWhenDelete()
    }
    
    @IBAction func deleteThirdImage(_ sender: Any) {
        imagePosition = 2
        replacePicturesWhenDelete()
    }
    
    @IBAction func deleteFourthImage(_ sender: Any) {
        imagePosition = 3
        replacePicturesWhenDelete()
    }
    
    func replacePicturesWhenDelete(){
        if(modifiedPhotosArray.indices.contains(imagePosition)){
            modifiedPhotosArray.remove(at: imagePosition)
            print("count on delete \(modifiedPhotosArray.count)")
            imagesFilled = [false,false,false,false]
            for ind in 0...modifiedPhotosArray.count{
                switch ind{
                case 0:
                    if(modifiedPhotosArray.indices.contains(0)){
                        firstImageView.image = modifiedPhotosArray[0]
                        imagesFilled[0] = true
                    }
                    else{
                        firstImageView.image = nil
                    }
                    break;
                case 1:
                    if(modifiedPhotosArray.indices.contains(1)){
                        secondImageView.image = modifiedPhotosArray[1]
                        imagesFilled[1] = true
                    }
                    else{
                        secondImageView.image = nil
                    }
                    break;
                case 2:
                    if(modifiedPhotosArray.indices.contains(2)){
                        thirdImageView.image = modifiedPhotosArray[2]
                        imagesFilled[2] = true
                    }
                    else{
                        thirdImageView.image = nil
                    }
                    
                    break;
                default://3
                    if(modifiedPhotosArray.indices.contains(3)){
                        fourthImageView.image = modifiedPhotosArray[3]
                        imagesFilled[3] = true
                    }
                    else{
                        fourthImageView.image = nil
                    }
                    break;
                }
            }
        }
        print(imagesFilled)
        print(modifiedPhotosArray)
    }
    
    func clickToUpload() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        switch imagePosition{
        case 0:
            firstImageView.image = selectedImage
            if(modifiedPhotosArray.indices.contains(0)){
                modifiedPhotosArray[0] = selectedImage
            }
            else{
                modifiedPhotosArray.append(selectedImage)
            }
            imagesFilled[0] = true
            break;
        case 1:
            if(imagesFilled[0]){
                secondImageView.image = selectedImage
                imagesFilled[1] = true
                if(modifiedPhotosArray.indices.contains(1)){
                    modifiedPhotosArray[1] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)

                }
            }
            else{
                firstImageView.image = selectedImage
                imagesFilled[0] = true
                modifiedPhotosArray[0] = selectedImage
            }
            break;
        case 2:
            if(imagesFilled[0] && imagesFilled[1]){
                thirdImageView.image = selectedImage
                imagesFilled[2] = true
                if(modifiedPhotosArray.indices.contains(2)){
                    modifiedPhotosArray[2] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)
                    
                }
            }
            else if(imagesFilled[0]){
                secondImageView.image = selectedImage
                imagesFilled[1] = true
                //modifiedPhotosArray.append(selectedImage)
                if(modifiedPhotosArray.indices.contains(1)){
                    modifiedPhotosArray[1] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)
                    
                }
            }
            else{
                firstImageView.image = selectedImage
                imagesFilled[0] = true
                modifiedPhotosArray[0] = selectedImage
            }
            break;
        default://3
            if(imagesFilled[2] && imagesFilled[1] && imagesFilled[0]){
                fourthImageView.image = selectedImage
                imagesFilled[3] = true
                if(modifiedPhotosArray.indices.contains(3)){
                    modifiedPhotosArray[3] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)
                    
                }
            }
            else if(imagesFilled[1] && imagesFilled[0]){
                thirdImageView.image = selectedImage
                imagesFilled[2] = true
                if(modifiedPhotosArray.indices.contains(2)){
                    modifiedPhotosArray[2] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)
                    
                }            }
            else if(imagesFilled[0]){
                secondImageView.image = selectedImage
                imagesFilled[1] = true
                
                if(modifiedPhotosArray.indices.contains(1)){
                    modifiedPhotosArray[1] = selectedImage
                }
                else{
                    modifiedPhotosArray.append(selectedImage)
                    
                }            }
            else{
                firstImageView.image = selectedImage
                imagesFilled[0] = true
                modifiedPhotosArray[0] = selectedImage
            }
            break;
        }
        print(modifiedPhotosArray.count)
        print(imagesFilled)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func hommeCheckboxClick(_ sender: Any) {
        if(hommeCheckboxChecked){
            hommeCheckbox.setImage(uncheckedBox, for: UIControl.State.normal)
            hommeCheckboxChecked = false
        }
        else{
            hommeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            hommeCheckboxChecked = true
        }
    }
    @IBAction func femmeCheckboxClick(_ sender: Any) {
        if(femmeCheckboxChecked){
            femmeCheckbox.setImage(uncheckedBox, for: UIControl.State.normal)
            femmeCheckboxChecked = false
        }
        else{
            femmeCheckbox.setImage(checkedBox, for: UIControl.State.normal)
            femmeCheckboxChecked = true
            
        }
    }
    
    @IBAction func onChangeDistanceValue(_ sender: UISlider) {
        sender.setValue(sender.value.rounded(.down), animated: true)
        distanceLabel.text = "\(Int(sender.value)) km"
        
    }
    
    @IBAction func onChangeAgeRange(_ sender: RangeSlider) {
        ageMinLabel.text = String( Int(ageRangeSlider.lowerValue))
        if(ageRangeSlider.upperValue == 35.0){
           ageMaxLabel.text = "35 +"
        }
        else{
            ageMaxLabel.text = String(Int(ageRangeSlider.upperValue))
        }
    }
    
    @IBAction func validerChangements(_ sender: Any) {
        ToastView.shared.short(self.view, txt_msg: "Changements enregistrés")
        
        if(hommeCheckboxChecked && !femmeCheckboxChecked){
            selectedPreference = 0
        }
        else if(femmeCheckboxChecked && !hommeCheckboxChecked){
            selectedPreference = 1
        }
        else{//il a coché les deux ou il a rien coché, donc par défaut on va y donner 2
            selectedPreference = 2
        }
        print(selectedPreference!)
        print(nameTextField.text)
        print(descriptionField.text)
        print(Int(ageRangeSlider.lowerValue))
        print(Int(ageRangeSlider.upperValue))
        print(Int(distanceSlider.value))
        updatedValues.prenom = nameTextField.text!
        updatedValues.description = descriptionField.text!
        updatedValues.preference = selectedPreference!
        updatedValues.age_min = Int(ageRangeSlider.lowerValue)
        updatedValues.age_max = Int(ageRangeSlider.upperValue)
        updatedValues.distance_max = Int(distanceSlider.value)
        
        let db = DatabaseAccessor.sharedInstance
        db.updateUser(user_id: authenticated_user.id, new_values: updatedValues)
    }
    @IBAction func valider(_ sender: UIButton) {
        ToastView.shared.short(self.view, txt_msg: "Changements enregistrés")
        
        if(hommeCheckboxChecked && !femmeCheckboxChecked){
            selectedPreference = 0
        }
        else if(femmeCheckboxChecked && !hommeCheckboxChecked){
            selectedPreference = 1
        }
        else{//il a coché les deux ou il a rien coché, donc par défaut on va y donner 2
            selectedPreference = 2
        }
        print(selectedPreference!)
        print(nameTextField.text)
        print(descriptionField.text)
        print(Int(ageRangeSlider.lowerValue))
        print(Int(ageRangeSlider.upperValue))
        print(Int(distanceSlider.value))
        updatedValues.prenom = nameTextField.text!
        updatedValues.description = descriptionField.text!
        updatedValues.preference = selectedPreference!
        updatedValues.age_min = Int(ageRangeSlider.lowerValue)
        updatedValues.age_max = Int(ageRangeSlider.upperValue)
        updatedValues.distance_max = Int(distanceSlider.value)
        //ptet inutile
        var encodedPicturesToUpload = Array<String>()
        for picture in modifiedPhotosArray{
            let toAdd = encodeFromUIImage(toEncode: picture)
            encodedPicturesToUpload.append(toAdd)
        }
        updatedValues.photos = encodedPicturesToUpload
        
        let db = DatabaseAccessor.sharedInstance
        db.updateUser(user_id: authenticated_user.id, new_values: updatedValues)
        
        db.updatePicturesOfUser(person_id: authenticated_user.id, initial_pictures: authenticated_user.photos, new_pictures: updatedValues.photos)
        //DEBUT TESTS
        //fourthImageView.image = returnImageFromEncodedString(encodedString: encodedPicturesToUpload[0])
        //db.updatePictureAtPosition(person_id: 1, photo_position: 1, encoded_photo: encodedPicturesToUpload[0])
       
        
        
        /*let imageData : Data  = modifiedPhotosArray[1].pngData()!
        print(imageData.description)
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        //let strBase64 = firstImageView.image.base64EncodedString(options: .lineLength64Characters)
        let decoded = returnImageFromEncodedString(encodedString: strBase64)
        fourthImageView.image = decoded*/
        
    }
    
    func encodeFromUIImage(toEncode: UIImage) -> String {// doit etre du png
        let imageData : Data  = toEncode.pngData()!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
    
}


